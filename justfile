# interactive menu to choose recipe
default:
	@just --choose

# open project
web:
	#!/bin/bash
	glab repo view -w
	xdotool key alt+1

# copy recipes docs
docs:
  #!/bin/bash
  just -l | xclip -sel clip
  nvim README.md +/Available recipes:

# build your caddy container
build:
  docker build -t mycaddy .

# deploy caddy 
deploy: build
  #!/bin/bash
  docker stack deploy caddy --compose-file docker-compose.yml
  firefox http://localhost/ &

# get service 
service name replica="1":
  #!/bin/bash
  docker ps -f "name=caddy_{{name}}.{{replica}}" --format="{{{{.Names}}"

# show logs 
logs name:
  #!/bin/bash
  docker logs "$(just service {{name}})"

# execute bash shell 
exec service:
  #!/bin/bash
  docker exec -it $(just service {{service}}) bash

# list all services
ls:  
  #!/bin/bash
  docker service ls

# kill and remove caddy 
remove:
  #!/bin/bash
  docker stack rm caddy 
