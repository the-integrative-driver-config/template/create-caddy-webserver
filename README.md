# Create caddy webserver

This project template is for creating caddy web servers

## Tools
- [just](https://github.com/casey/just)
- [xclip](https://github.com/astrand/xclip)
- [neovim](https://github.com/neovim/neovim)
- [caddy](https://github.com/caddyserver/caddy)

## Usage
``` shell
Available recipes:
    build                    # Build your caddy container
    default                  # Interactive menu to choose recipe
    deploy                   # deploy caddy
    docs                     # copy recipes docs
    exec service             # execute bash shell
    logs name                # show logs
    ls                       # list all services
    remove                   # kill and remove caddy
    service name replica="1" # get service
    web                      # open project
```
